class Fact < ApplicationRecord
  include HTTParty
  base_uri 'https://api.chucknorris.io/jokes/search?query='
  def self.for query
    myFact = Fact.new
    @response = HTTParty.get(base_uri+query, format: :json)
    myFact.searched = query
    @elements = @response.parsed_response["result"]
    if !@elements.nil?
      myFact.response = @elements
      @answered = @elements.collect { |e| e['value'] }
    end
    if !@elements.nil? and !@answered.nil?
      myFact.save!
      return @answered
    end
  end
  def self.random
    @response = HTTParty.get('https://api.chucknorris.io/jokes/random', format: :json)
    @fact = @response.parsed_response['value']
  end
end