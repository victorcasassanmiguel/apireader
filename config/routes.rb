Rails.application.routes.draw do
  get 'facts/index'
  root to: 'facts#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
