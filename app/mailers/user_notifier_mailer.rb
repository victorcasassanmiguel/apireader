class UserNotifierMailer < ApplicationMailer
  def to_email(answered, email)
    @answered = answered
    mail(to: email, subject:  'Found Chuck Norris Facts')
  end
end
