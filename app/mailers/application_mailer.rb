class ApplicationMailer < ActionMailer::Base
  default from: 'victor.casas@apireader.herokuapp.com'
end
