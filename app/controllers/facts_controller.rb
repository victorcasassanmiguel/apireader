class FactsController < ApplicationController
  require 'will_paginate/array'
  def index
    if !params[:searched].nil? && !params[:searched].empty?
       @email = params[:email]
       @searched = params[:searched]
       @answered = Fact.for params[:searched]
       if !@answered.nil?
         if !params[:email].empty?
           UserNotifierMailer.to_email(@answered, @email).deliver_now
         end
         @answered = @answered.paginate(:page => params[:page], :per_page => 10)
       else
         @fact = Fact.random
       end
    else
      @fact = Fact.random
    end
  end
end